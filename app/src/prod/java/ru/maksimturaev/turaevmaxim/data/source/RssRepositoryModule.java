package ru.maksimturaev.turaevmaxim.data.source;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.maksimturaev.turaevmaxim.data.Api;
import ru.maksimturaev.turaevmaxim.data.source.local.RssLocalDataSource;
import ru.maksimturaev.turaevmaxim.data.source.remote.RssRemoteDataSource;

/**
 * This is used by Dagger to inject the required arguments into the {@link RssRepository}.
 */
@Module
public class RssRepositoryModule {

    @Singleton
    @Provides
    @Local
    RssDataSource provideRssLocalDataSource(Context context) {
        return new RssLocalDataSource(context);
    }

    @Singleton
    @Provides
    @Remote
    RssDataSource provideRssRemoteDataSource(Api api) {
        return new RssRemoteDataSource(api);
    }

}
