/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakeRssRemoteDataSource implements RssDataSource {

    private static final Map<String, RssItem> FEED_SERVICE_DATA = new LinkedHashMap<>();

    public FakeRssRemoteDataSource() {}

    public void getFeed(@NonNull LoadFeedCallback callback) {
        callback.onFeedLoaded(new ArrayList<RssItem>(FEED_SERVICE_DATA.values()));
    }

    @Override
    public void getRssItem(@NonNull String RssItemId, @NonNull RssDataSource.GetRssItemCallback callback) {
        RssItem rssItem = FEED_SERVICE_DATA.get(RssItemId);
        callback.onRssItemLoaded(rssItem);
    }

    @Override
    public void saveRssItem(@NonNull RssItem rssItem) {
        FEED_SERVICE_DATA.put(rssItem.getId(), rssItem);
    }

    @Override
    public void saveFeed(@NonNull List<RssItem> rssItems) {
        for (RssItem rssItem : rssItems) {
            FEED_SERVICE_DATA.put(rssItem.getId(), rssItem);
        }
    }


    public void refreshFeed() {
        // Not required because the {@link RssRepository} handles the logic of refreshing the
        // tasks from all the available data sources.
    }

    @Override
    public void deleteRssItem(@NonNull String RssItemId) {
        FEED_SERVICE_DATA.remove(RssItemId);
    }

    public void deleteFeed() {
        FEED_SERVICE_DATA.clear();
    }

    @VisibleForTesting
    public void addRssItems(RssItem... rssItems) {
        for (RssItem rssItem : rssItems) {
            FEED_SERVICE_DATA.put(rssItem.getId(), rssItem);
        }
    }
}
