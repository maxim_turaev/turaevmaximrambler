/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;
import ru.maksimturaev.turaevmaxim.data.source.local.RssLocalDataSource;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Integration test for the {@link RssDataSource}, which uses the {@link RssDbHelper}.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class RssLocalDataSourceTest {

    private final static String TITLE = "title";

    private final static String TITLE2 = "title2";

    private final static String TITLE3 = "title3";

    private RssLocalDataSource mLocalDataSource;

    @Before
    public void setup() {
         mLocalDataSource = new RssLocalDataSource(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void cleanUp() {
        mLocalDataSource.deleteFeed();
    }

    @Test
    public void testPreConditions() {
        assertNotNull(mLocalDataSource);
    }

    @Test
    public void saveRssItem_retrievesRssItem() {
        // Given a new rss item
        final RssItem newRssItem = new RssItem(TITLE, "", "", "");

        // When saved into the persistent repository
        mLocalDataSource.saveRssItem(newRssItem);

        // Then the rssIterm can be retrieved from the persistent repository
        mLocalDataSource.getRssItem(newRssItem.getId(), new RssDataSource.GetRssItemCallback() {
            @Override
            public void onRssItemLoaded(RssItem rssItem) {
                assertThat(rssItem, is(newRssItem));
            }

            @Override
            public void onDataNotAvailable() {
                fail("Callback error");
            }
        });
    }

    @Test
    public void deleteAllFeed_emptyListOfRetrievedRssItem() {
        // Given a new rss item in the persistent repository and a mocked callback
        RssItem newRssItem = new RssItem(TITLE, "", "", "");
        mLocalDataSource.saveRssItem(newRssItem);
        RssDataSource.LoadFeedCallback callback = mock(RssDataSource.LoadFeedCallback.class);

        // When all rss items are deleted
        mLocalDataSource.deleteFeed();

        // Then the retrieved rss items is an empty list
        mLocalDataSource.getFeed(callback);

        verify(callback).onDataNotAvailable();
        verify(callback, never()).onFeedLoaded(anyList());
    }

    @Test
    public void getFeed_retrieveSavedFeed() {
        // Given 2 new rss items in the persistent repository
        final RssItem newRssItem1 = new RssItem(TITLE, "", "", "");
        mLocalDataSource.saveRssItem(newRssItem1);
        final RssItem newRssItem2 = new RssItem(TITLE, "", "", "");
        mLocalDataSource.saveRssItem(newRssItem2);

        // Then the rss items can be retrieved from the persistent repository
        mLocalDataSource.getFeed(new RssDataSource.LoadFeedCallback() {
            @Override
            public void onFeedLoaded(List<RssItem> rssItems) {
                assertNotNull(rssItems);
                assertTrue(rssItems.size() >= 2);

                boolean newRssItem1IdFound = false;
                boolean newRssItem2IdFound = false;
                for (RssItem rssItem : rssItems) {
                    if (rssItem.getId().equals(newRssItem1.getId())) {
                        newRssItem1IdFound = true;
                    }
                    if (rssItem.getId().equals(newRssItem2.getId())) {
                        newRssItem2IdFound = true;
                    }
                }
                assertTrue(newRssItem1IdFound);
                assertTrue(newRssItem2IdFound);
            }

            @Override
            public void onDataNotAvailable() {
                fail();
            }
        });
    }
}
