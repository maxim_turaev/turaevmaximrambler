/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data.source;

import android.content.Context;


import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import ru.maksimturaev.turaevmaxim.data.RssItem;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
public class RssRepositoryTest {

    private final static String RSS_ITEM_TITLE = "title";

    private final static String RSS_ITEM_TITLE2 = "title2";

    private final static String RSS_ITEM_TITLE3 = "title3";

    private static List<RssItem> FEED = new ArrayList<RssItem>() {{
        add(new RssItem("Title1", "Description1", "Wed, 23 Nov 2016 12:06:00 +0300", "url"));
        add(new RssItem("Title2", "Description2", "Wed, 23 Nov 2016 12:06:00 +0300", "url"));
    }};

    private RssRepository rssRepository;

    @Mock
    private RssDataSource rssRemoteDataSource;

    @Mock
    private RssDataSource rssLocalDataSource;

    @Mock
    private Context mContext;

    @Mock
    private RssDataSource.GetRssItemCallback mGetRssItemCallback;

    @Mock
    private RssDataSource.LoadFeedCallback mLoadFeedCallback;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<RssDataSource.LoadFeedCallback> feedCallbackCaptor;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<RssDataSource.GetRssItemCallback> rssItemCallbackCaptor;

    @Before
    public void setupRssRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        rssRepository = new RssRepository(rssRemoteDataSource, rssLocalDataSource);
    }

    @Test
    public void getFeed_repositoryCachesAfterFirstApiCall() {
        // Given a setup Captor to capture callbacks
        // When two calls are issued to the rss items repository
        twoRssItemsLoadCallsToRepository(mLoadFeedCallback);

        // Then rss items were only requested once from Service API
        verify(rssRemoteDataSource).getFeed(any(RssDataSource.LoadFeedCallback.class));
    }

    @Test
    public void getFeed_requestsAllRssItemsFromLocalDataSource() {
        // When rss items are requested from the rss items repository
        rssRepository.getFeed(mLoadFeedCallback);

        // Then rss items are loaded from the local data source
        verify(rssLocalDataSource).getFeed(any(RssDataSource.LoadFeedCallback.class));
    }

    @Test
    public void saveRssItem_savesRssItemToServiceAPI() {
        // Given a stub rss item with title and description
        RssItem newRssItem = new RssItem(RSS_ITEM_TITLE, "Some RssItem Description", "Some Date", "Some Url");

        // When a rss item is saved to the rss items repository
        rssRepository.saveRssItem(newRssItem);

        // Then the service API and persistent repository are called and the cache is updated
        verify(rssRemoteDataSource).saveRssItem(newRssItem);
        verify(rssLocalDataSource).saveRssItem(newRssItem);
        assertThat(rssRepository.cachedFeed.size(), is(1));
    }

    @Test
    public void getRssItem_requestsSingleRssItemFromLocalDataSource() {
        // When a rss item is requested from the rss items repository
        rssRepository.getRssItem(RSS_ITEM_TITLE, mGetRssItemCallback);

        // Then the rss item is loaded from the database
        verify(rssLocalDataSource).getRssItem(eq(RSS_ITEM_TITLE), any(
                RssDataSource.GetRssItemCallback.class));
    }

    @Test
    public void deleteAllRssItems_deleteRssItemsToServiceAPIUpdatesCache() {
        RssItem newRssItem = new RssItem(RSS_ITEM_TITLE, "Some RssItem Description", "Some Date", "Some Url");
        rssRepository.saveRssItem(newRssItem);
        RssItem newRssItem2 = new RssItem(RSS_ITEM_TITLE2, "Some RssItem Description", "Some Date", "Some Url");
        rssRepository.saveRssItem(newRssItem2);
        RssItem newRssItem3 = new RssItem(RSS_ITEM_TITLE3, "Some RssItem Description", "Some Date", "Some Url");
        rssRepository.saveRssItem(newRssItem3);

        // When all rss items are deleted to the rss items repository
        rssRepository.deleteFeed();

        // Verify the data sources were called
        verify(rssRemoteDataSource).deleteFeed();
        verify(rssLocalDataSource).deleteFeed();

        assertThat(rssRepository.cachedFeed.size(), is(0));
    }

    @Test
    public void deleteRssItem_deleteRssItemToServiceAPIRemovedFromCache() {
        // Given a rss item in the repository
        RssItem newRssItem = new RssItem(RSS_ITEM_TITLE, "Some RssItem Description", "Some Date", "Some Url");
        rssRepository.saveRssItem(newRssItem);
        assertThat(rssRepository.cachedFeed.containsKey(newRssItem.getId()), is(true));

        // When deleted
        rssRepository.deleteRssItem(newRssItem.getId());

        // Verify the data sources were called
        verify(rssRemoteDataSource).deleteRssItem(newRssItem.getId());
        verify(rssLocalDataSource).deleteRssItem(newRssItem.getId());

        // Verify it's removed from repository
        assertThat(rssRepository.cachedFeed.containsKey(newRssItem.getId()), is(false));
    }

    @Test
    public void getRssItemsWithDirtyCache_rssItemsAreRetrievedFromRemote() {
        // When calling getFeed in the repository with dirty cache
        rssRepository.refreshFeed();
        rssRepository.getFeed(mLoadFeedCallback);

        // And the remote data source has data available
        setFeedAvailable(rssRemoteDataSource, FEED);

        // Verify the rss items from the remote data source are returned, not the local
        verify(rssLocalDataSource, never()).getFeed(mLoadFeedCallback);
        verify(mLoadFeedCallback).onFeedLoaded(FEED);
    }

    @Test
    public void getRssItemsWithLocalDataSourceUnavailable_rssItemsAreRetrievedFromRemote() {
        // When calling getFeed in the repository
        rssRepository.getFeed(mLoadFeedCallback);

        // And the local data source has no data available
        setFeedNotAvailable(rssLocalDataSource);

        // And the remote data source has data available
        setFeedAvailable(rssRemoteDataSource, FEED);

        // Verify the rss items from the local data source are returned
        verify(mLoadFeedCallback).onFeedLoaded(FEED);
    }

    @Test
    public void getFeedWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // When calling getFeed in the repository
        rssRepository.getFeed(mLoadFeedCallback);

        // And the local data source has no data available
        setFeedNotAvailable(rssLocalDataSource);

        // And the remote data source has no data available
        setFeedNotAvailable(rssRemoteDataSource);

        // Verify no data is returned
        verify(mLoadFeedCallback).onDataNotAvailable();
    }

    @Test
    public void getRssItemWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given a rss item id
        final String rssItemId = "123";

        // When calling getRssItem in the repository
        rssRepository.getRssItem(rssItemId, mGetRssItemCallback);

        // And the local data source has no data available
        setRssItemNotAvailable(rssLocalDataSource, rssItemId);

        // And the remote data source has no data available
        setRssItemNotAvailable(rssRemoteDataSource, rssItemId);

        // Verify no data is returned
        verify(mGetRssItemCallback).onDataNotAvailable();
    }

    @Test
    public void getFeed_refreshesLocalDataSource() {
        // Mark cache as dirty to force a reload of data from remote data source.
        rssRepository.refreshFeed();

        // When calling getFeed in the repository
        rssRepository.getFeed(mLoadFeedCallback);

        // Make the remote data source return data
        setFeedAvailable(rssRemoteDataSource, FEED);

        // Verify that the data fetched from the remote data source was saved in local.
        verify(rssLocalDataSource, times(1)).saveFeed(anyListOf(RssItem.class));
    }

    /**
     * Convenience method that issues two calls to the rss items repository
     */
    private void twoRssItemsLoadCallsToRepository(RssDataSource.LoadFeedCallback callback) {
        // When rss items are requested from repository
        rssRepository.getFeed(callback); // First call to API

        // Use the Mockito Captor to capture the callback
        verify(rssLocalDataSource).getFeed(feedCallbackCaptor.capture());

        // Local data source doesn't have data yet
        feedCallbackCaptor.getValue().onDataNotAvailable();


        // Verify the remote data source is queried
        verify(rssRemoteDataSource).getFeed(feedCallbackCaptor.capture());

        // Trigger callback so rss items are cached
        feedCallbackCaptor.getValue().onFeedLoaded(FEED);

        rssRepository.getFeed(callback); // Second call to API
    }

    private void setFeedNotAvailable(RssDataSource dataSource) {
        verify(dataSource).getFeed(feedCallbackCaptor.capture());
        feedCallbackCaptor.getValue().onDataNotAvailable();
    }

    private void setFeedAvailable(RssDataSource dataSource, List<RssItem> rssItems) {
        verify(dataSource).getFeed(feedCallbackCaptor.capture());
        feedCallbackCaptor.getValue().onFeedLoaded(rssItems);
    }

    private void setRssItemNotAvailable(RssDataSource dataSource, String RssItemId) {
        verify(dataSource).getRssItem(eq(RssItemId), rssItemCallbackCaptor.capture());
        rssItemCallbackCaptor.getValue().onDataNotAvailable();
    }

    private void setRssItemAvailable(RssDataSource dataSource, RssItem rssItem) {
        verify(dataSource).getRssItem(eq(rssItem.getId()), rssItemCallbackCaptor.capture());
        rssItemCallbackCaptor.getValue().onRssItemLoaded(rssItem);
    }
}
