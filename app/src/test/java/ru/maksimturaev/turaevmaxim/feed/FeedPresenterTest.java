/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.feed;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import ru.maksimturaev.turaevmaxim.data.RssItem;
import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;
import ru.maksimturaev.turaevmaxim.data.source.RssRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link FeedPresenter}
 */
public class FeedPresenterTest {

    private static List<RssItem> FEED;

    @Mock
    private RssRepository rssRepository;

    @Mock
    private FeedContract.View feedView;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<RssDataSource.LoadFeedCallback> mLoadFeedCallbackCaptor;

    private FeedPresenter mFeedPresenter;

    @Before
    public void setupFeedPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mFeedPresenter = new FeedPresenter(rssRepository, feedView);

        // The presenter won't update the view unless it's active.
        when(feedView.isActive()).thenReturn(true);

        // We start the rss items to 3, with one active and two completed
        FEED = new ArrayList<RssItem>() {{
            add(new RssItem("Title1", "Description1", "Date1", "Url1"));
            add(new RssItem("Title2", "Description2", "Date2", "Url2"));
            add(new RssItem("Title3", "Description3", "Date3", "Url3"));
        }};
    }

    @Test
    public void clickOnRssItem_ShowsDetailUi() {
        // Given a stubbed active rss item
        RssItem requestedRssItem = new RssItem("Details Requested", "For this rss item", "Date", "Url");

        // When open rss item details is requested
        mFeedPresenter.openRssItemDetails(requestedRssItem);

        // Then rss item detail UI is shown
        verify(feedView).showRssItemDetailsUi(any(String.class));
    }


    @Test
    public void unavailableFeed_ShowsError() {
        // When rss items are loaded
        mFeedPresenter.loadFeed(true);

        // And the rss items aren't available in the repository
        verify(rssRepository).getFeed(mLoadFeedCallbackCaptor.capture());
        mLoadFeedCallbackCaptor.getValue().onDataNotAvailable();

        // Then an error message is shown
        verify(feedView).showLoadingFeedError();
    }
}
