package ru.maksimturaev.turaevmaxim.util;

public class Strings {
    public static boolean isNullOrEmpty(String s) {
        return !(s != null && !s.isEmpty());
    }
}
