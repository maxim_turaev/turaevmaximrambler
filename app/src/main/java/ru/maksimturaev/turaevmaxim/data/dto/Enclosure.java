package ru.maksimturaev.turaevmaxim.data.dto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "enclosure", strict = false)
public class Enclosure {

    public Enclosure() {
    }

    @Attribute(required = false)
    private String url;

    public String getUrl() {
        return url;
    }
}
