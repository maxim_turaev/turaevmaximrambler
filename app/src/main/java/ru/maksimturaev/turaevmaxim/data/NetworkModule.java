package ru.maksimturaev.turaevmaxim.data;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class NetworkModule {

    private final String handedUrl;

    public NetworkModule(String handedUrl) {
        this.handedUrl = handedUrl;
    }

    @Singleton
    @Provides
    HttpUrl provideEndPoint() {
        return HttpUrl.parse(handedUrl);
    }

    @Singleton
    @Provides
    public OkHttpClient provideHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        return builder.build();
    }

    @Singleton
    @Provides
    public Api provideFeedbackApi(OkHttpClient client, HttpUrl endpoint) {
        Retrofit restAdapter =
                new Retrofit.Builder()
                        .baseUrl(endpoint)
                        .addConverterFactory(SimpleXmlConverterFactory.create())
                        .client(client).build();
        Api rssApi = restAdapter.create(Api.class);
        return rssApi;
    }
}
