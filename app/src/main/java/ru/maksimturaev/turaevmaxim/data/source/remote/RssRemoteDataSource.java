/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data.source.remote;

import android.os.Handler;
import android.support.annotation.NonNull;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.maksimturaev.turaevmaxim.data.Api;
import ru.maksimturaev.turaevmaxim.data.RssItem;
import ru.maksimturaev.turaevmaxim.data.dto.Feed;
import ru.maksimturaev.turaevmaxim.data.dto.FeedItem;
import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;
import timber.log.Timber;

/**
 * Implementation of the data source that adds a latency simulating network.
 */
@Singleton
public class RssRemoteDataSource implements RssDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 5000;

    private final static Map<String, RssItem> FEED_SERVICE_DATA;

    static {
        FEED_SERVICE_DATA = new LinkedHashMap<>(2);
        addRssItem("Build tower in Pisa", "Ground looks good, no foundation work required.", "Wed, 23 Nov 2016 12:06:00 +0300", "http://www.fnordware.com/superpng/pngtest8rgba.png");
        addRssItem("Finish bridge in Tacoma", "Found awesome girders at half the cost!", "Wed, 23 Nov 2016 12:16:00 +0300", "http://www.fnordware.com/superpng/pngtest8rgba.png");
    }

    private Api api;

    @Inject
    public RssRemoteDataSource(Api api) {
        this.api = api;
    }

    private static void addRssItem(String title, String description, String date, String imageUrl) {
        RssItem newRssItem = new RssItem(title, description, date, imageUrl);
        FEED_SERVICE_DATA.put(newRssItem.getId(), newRssItem);
    }

    /**
     * Note: {@link LoadFeedCallback#onDataNotAvailable()} is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */
    @Override
    public void getFeed(final @NonNull LoadFeedCallback callback) {
        Call<Feed> feed = api.getFeed();
        feed.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                List<RssItem> resultList = new ArrayList<>();
                for (FeedItem feedItem : response.body().getChannel().getFeedItems()) {
                    String imageUrl = null;
                    if (feedItem.getLink() != null) {
                        imageUrl = feedItem.getLink().getUrl();
                    }
                    resultList.add(new RssItem(feedItem.getTitle(), feedItem.getDescription(), feedItem.getPubDate(), imageUrl));
                }
                callback.onFeedLoaded(resultList);
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                Timber.e(t);
                callback.onDataNotAvailable();
            }
        });
//        // Simulate network by delaying the execution.
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                callback.onFeedLoaded(new ArrayList<RssItem>(FEED_SERVICE_DATA.values()));
//            }
//        }, SERVICE_LATENCY_IN_MILLIS);
    }

    /**
     * Note: {@link GetRssItemCallback#onDataNotAvailable()} is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */
    @Override
    public void getRssItem(@NonNull String rssItemId, final @NonNull GetRssItemCallback callback) {
        final RssItem rssItem = FEED_SERVICE_DATA.get(rssItemId);


        api.getFeed();
        // Simulate network by delaying the execution.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onRssItemLoaded(rssItem);
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveRssItem(@NonNull RssItem rssItem) {
        FEED_SERVICE_DATA.put(rssItem.getId(), rssItem);
    }

    @Override
    public void saveFeed(@NonNull List<RssItem> rssItems) {
        throw new NoSuchMethodError();
    }

    @Override
    public void refreshFeed() {
        // Not required because the {@link RssRepository} handles the logic of refreshing the
        // tasks from all the available data sources.
    }

    @Override
    public void deleteFeed() {
        FEED_SERVICE_DATA.clear();
    }

    @Override
    public void deleteRssItem(@NonNull String rssItemId) {
        FEED_SERVICE_DATA.remove(rssItemId);
    }
}
