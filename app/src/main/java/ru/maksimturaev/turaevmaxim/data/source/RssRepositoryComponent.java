package ru.maksimturaev.turaevmaxim.data.source;

import javax.inject.Singleton;

import dagger.Component;
import ru.maksimturaev.turaevmaxim.ApplicationModule;
import ru.maksimturaev.turaevmaxim.data.NetworkModule;

/**
 * This is a Dagger component. Refer to {@link RssApplication} for the list of Dagger components
 * used in this application.
 * <P>
 * Even though Dagger allows annotating a {@link Component @Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * RssApplication}.
 */
@Singleton
@Component(modules = {RssRepositoryModule.class, ApplicationModule.class, NetworkModule.class})
public interface RssRepositoryComponent {

    RssRepository getRssRepository();
}
