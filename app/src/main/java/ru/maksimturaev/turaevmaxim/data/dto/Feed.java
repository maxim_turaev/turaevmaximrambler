package ru.maksimturaev.turaevmaxim.data.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class Feed {
    @Element(name = "channel")
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }

    public Feed() {
    }

    public Feed(Channel channel) {
        this.channel = channel;
    }
}