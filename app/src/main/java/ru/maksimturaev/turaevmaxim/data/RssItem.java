/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

import static ru.maksimturaev.turaevmaxim.util.Strings.isNullOrEmpty;

/**
 * Immutable model class for a RssItem.
 */
public final class RssItem {

    @NonNull
    private final String id;

    @Nullable
    private final String title;

    @Nullable
    private final String description;

    @Nullable
    private final String pubDate;

    @Nullable
    private final String imageUrl;

    /**
     * Use this constructor to create a new active RssItem.
     *
     * @param title       title of the rss item
     * @param description description of the rss item
     */
    public RssItem(@Nullable String title, @Nullable String description, @Nullable String pubDate, @Nullable String imageUrl) {
        this(title, description, pubDate, imageUrl, UUID.randomUUID().toString());
    }

    /**
     * Use this constructor to specify a completed RssItem if the RssItem already has an id (copy of
     * another RssItem).
     *
     * @param title       title of the rss item
     * @param description description of the rss item
     * @param id          id of the rss item
     */
    public RssItem(@Nullable String title, @Nullable String description, @Nullable String pubDate, @Nullable String imageUrl,
                   @NonNull String id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.imageUrl = imageUrl;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getTitleForList() {
        if (!isNullOrEmpty(title)) {
            return title;
        } else {
            return description;
        }
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public String getPubDate() {
        return pubDate;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isEmpty() {
        return isNullOrEmpty(title) &&
                isNullOrEmpty(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RssItem)) return false;

        RssItem rssItem = (RssItem) o;

        if (!id.equals(rssItem.id)) return false;
        if (title != null ? !title.equals(rssItem.title) : rssItem.title != null) return false;
        if (description != null ? !description.equals(rssItem.description) : rssItem.description != null)
            return false;
        if (pubDate != null ? !pubDate.equals(rssItem.pubDate) : rssItem.pubDate != null)
            return false;
        return imageUrl != null ? imageUrl.equals(rssItem.imageUrl) : rssItem.imageUrl == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (pubDate != null ? pubDate.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RssItem with title " + title;
    }
}
