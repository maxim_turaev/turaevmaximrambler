/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data.source;

import android.support.annotation.NonNull;


import java.util.List;

import ru.maksimturaev.turaevmaxim.data.RssItem;

/**
 * Main entry point for accessing rss items data.
 * <p>
 * For simplicity, only getFeed() and getRssItem() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new rss item is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */
public interface RssDataSource {

    interface LoadFeedCallback {

        void onFeedLoaded(List<RssItem> rssItems);

        void onDataNotAvailable();
    }

    interface GetRssItemCallback {

        void onRssItemLoaded(RssItem rssItem);

        void onDataNotAvailable();
    }

    void getFeed(@NonNull LoadFeedCallback callback);

    void getRssItem(@NonNull String rssItemId, @NonNull GetRssItemCallback callback);

    void saveRssItem(@NonNull RssItem rssItem);

    void saveFeed(@NonNull List<RssItem> rssItems);

    void refreshFeed();

    void deleteFeed();

    void deleteRssItem(@NonNull String rssItemId);
}
