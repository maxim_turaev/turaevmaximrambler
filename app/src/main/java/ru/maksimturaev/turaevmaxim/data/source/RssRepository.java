/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.maksimturaev.turaevmaxim.data.RssItem;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Concrete implementation to load rss items from the data sources into a cache.
 * <p>
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 * <p/>
 * By marking the constructor with {@code @Inject} and the class with {@code @Singleton}, Dagger
 * injects the dependencies required to create an instance of the RssRespository (if it fails, it
 * emits a compiler error). It uses {@link RssRepositoryModule} to do so, and the constructed
 * instance is available in {@link RssRepositoryComponent}.
 * <p/>
 * Dagger generated code doesn't require public access to the constructor or class, and
 * therefore, to ensure the developer doesn't instantiate the class manually and bypasses Dagger,
 * it's good practice minimise the visibility of the class/constructor as much as possible.
 */
@Singleton
public class RssRepository implements RssDataSource {

    private final RssDataSource rssRemoteDataSource;

    private final RssDataSource rssLocalDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    Map<String, RssItem> cachedFeed;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    boolean cacheIsDirty = false;

    /**
     * By marking the constructor with {@code @Inject}, Dagger will try to inject the dependencies
     * required to create an instance of the RssRepository. Because {@link RssDataSource} is an
     * interface, we must provide to Dagger a way to build those arguments, this is done in
     * {@link RssRepositoryModule}.
     * <p>
     * When two arguments or more have the same type, we must provide to Dagger a way to
     * differentiate them. This is done using a qualifier.
     * <p>
     * Dagger strictly enforces that arguments not marked with {@code @Nullable} are not injected
     * with {@code @Nullable} values.
     */
    @Inject
    RssRepository(@Remote RssDataSource rssRemoteDataSource,
                  @Local RssDataSource rssLocalDataSource) {
        this.rssRemoteDataSource = rssRemoteDataSource;
        this.rssLocalDataSource = rssLocalDataSource;
    }

    /**
     * Gets rss items from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     * <p>
     * Note: {@link LoadFeedCallback#onDataNotAvailable()} is fired if all data sources fail to
     * get the data.
     */
    @Override
    public void getFeed(@NonNull final LoadFeedCallback callback) {
        checkNotNull(callback);

        // Respond immediately with cache if available and not dirty
        if (cachedFeed != null && !cacheIsDirty) {
            callback.onFeedLoaded(new ArrayList<>(cachedFeed.values()));
            return;
        }

        if (cacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            getFeedFromRemoteDataSource(callback);
        } else {
            // Query the local storage if available. If not, query the network.
            rssLocalDataSource.getFeed(new LoadFeedCallback() {
                @Override
                public void onFeedLoaded(List<RssItem> rssItems) {
                    refreshCache(rssItems);
                    callback.onFeedLoaded(new ArrayList<>(cachedFeed.values()));
                }

                @Override
                public void onDataNotAvailable() {
                    getFeedFromRemoteDataSource(callback);
                }
            });
        }
    }

    @Override
    public void saveRssItem(@NonNull RssItem rssItem) {
        checkNotNull(rssItem);
        rssRemoteDataSource.saveRssItem(rssItem);
        rssLocalDataSource.saveRssItem(rssItem);

        // Do in memory cache update to keep the app UI up to date
        if (cachedFeed == null) {
            cachedFeed = new LinkedHashMap<>();
        }
        cachedFeed.put(rssItem.getId(), rssItem);
    }

    @Override
    public void saveFeed(@NonNull List<RssItem> rssItems) {
        rssLocalDataSource.saveFeed(rssItems);
        // Do in memory cache update to keep the app UI up to date
        if (cachedFeed == null) {
            cachedFeed = new LinkedHashMap<>();
        }
        for (RssItem rssItem : rssItems) {
            cachedFeed.put(rssItem.getId(), rssItem);
        }
    }

    /**
     * Gets rss items from local data source (sqlite) unless the table is new or empty. In that case it
     * uses the network data source. This is done to simplify the sample.
     * <p>
     * Note: {@link LoadFeedCallback#onDataNotAvailable()} is fired if both data sources fail to
     * get the data.
     */
    @Override
    public void getRssItem(@NonNull final String rssItemId, @NonNull final GetRssItemCallback callback) {
        checkNotNull(rssItemId);
        checkNotNull(callback);

        RssItem cachedRssItem = getRssItemWithId(rssItemId);

        // Respond immediately with cache if available
        if (cachedRssItem != null) {
            callback.onRssItemLoaded(cachedRssItem);
            return;
        }

        // Load from server/persisted if needed.

        // Is the rss item in the local data source? If not, query the network.
        rssLocalDataSource.getRssItem(rssItemId, new GetRssItemCallback() {
            @Override
            public void onRssItemLoaded(RssItem rssItem) {
                callback.onRssItemLoaded(rssItem);
            }

            @Override
            public void onDataNotAvailable() {
                rssRemoteDataSource.getRssItem(rssItemId, new GetRssItemCallback() {
                    @Override
                    public void onRssItemLoaded(RssItem rssItem) {
                        callback.onRssItemLoaded(rssItem);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }
                });
            }
        });
    }

    @Override
    public void refreshFeed() {
        cacheIsDirty = true;
    }

    @Override
    public void deleteFeed() {
        rssRemoteDataSource.deleteFeed();
        rssLocalDataSource.deleteFeed();

        if (cachedFeed == null) {
            cachedFeed = new LinkedHashMap<>();
        }
        cachedFeed.clear();
    }

    @Override
    public void deleteRssItem(@NonNull String rssItemId) {
        rssRemoteDataSource.deleteRssItem(checkNotNull(rssItemId));
        rssLocalDataSource.deleteRssItem(checkNotNull(rssItemId));

        cachedFeed.remove(rssItemId);
    }

    private void getFeedFromRemoteDataSource(@NonNull final LoadFeedCallback callback) {
        rssRemoteDataSource.getFeed(new LoadFeedCallback() {
            @Override
            public void onFeedLoaded(List<RssItem> rssItems) {
                refreshCache(rssItems);
                refreshLocalDataSource(rssItems);
                callback.onFeedLoaded(new ArrayList<>(cachedFeed.values()));
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    private void refreshCache(List<RssItem> rssItems) {
        if (cachedFeed == null) {
            cachedFeed = new LinkedHashMap<>();
        }
        cachedFeed.clear();
        for (RssItem rssItem : rssItems) {
            cachedFeed.put(rssItem.getId(), rssItem);
        }
        cacheIsDirty = false;
    }

    private void refreshLocalDataSource(List<RssItem> rssItems) {
        rssLocalDataSource.deleteFeed();
        rssLocalDataSource.saveFeed(rssItems);
    }

    @Nullable
    private RssItem getRssItemWithId(@NonNull String id) {
        checkNotNull(id);
        if (cachedFeed == null || cachedFeed.isEmpty()) {
            return null;
        } else {
            return cachedFeed.get(id);
        }
    }
}
