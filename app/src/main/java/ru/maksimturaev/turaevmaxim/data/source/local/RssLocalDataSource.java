/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.data.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import ru.maksimturaev.turaevmaxim.data.RssItem;
import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;

import static dagger.internal.Preconditions.checkNotNull;


/**
 * Concrete implementation of a data source as a db.
 */
@Singleton
public class RssLocalDataSource implements RssDataSource {

    private RssDbHelper dbHelper;

    public RssLocalDataSource(@NonNull Context context) {
        checkNotNull(context);
        dbHelper = new RssDbHelper(context);
    }

    /**
     * Note: {@link LoadFeedCallback#onDataNotAvailable()} is fired if the database doesn't exist
     * or the table is empty.
     */
    @Override
    public void getFeed(@NonNull RssDataSource.LoadFeedCallback callback) {
        List<RssItem> rssItems = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL
        };

        Cursor c = db.query(
                RssPersistenceContract.RssItemEntry.TABLE_NAME, projection, null, null, null, null, null);

        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                String itemId = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID));
                String title = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE));
                String description =
                        c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION));
                String pubDate = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE));
                String imageUrl =
                        c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL));
                RssItem rssItem = new RssItem(title, description, pubDate, imageUrl, itemId);
                rssItems.add(rssItem);
            }
        }
        if (c != null) {
            c.close();
        }

        db.close();

        if (rssItems.isEmpty()) {
            // This will be called if the table is new or just empty.
            callback.onDataNotAvailable();
        } else {
            callback.onFeedLoaded(rssItems);
        }

    }

    /**
     * Note: {@link GetRssItemCallback#onDataNotAvailable()} is fired if the {@link RssItem} isn't
     * found.
     */
    @Override
    public void getRssItem(@NonNull String rssItemId, @NonNull GetRssItemCallback callback) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE,
                RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL
        };

        String selection = RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {rssItemId};

        Cursor c = db.query(
                RssPersistenceContract.RssItemEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, null);

        RssItem rssItem = null;

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            String itemId = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID));
            String title = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE));
            String description =
                    c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION));
            String pubDate = c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE));
            String imageUrl =
                    c.getString(c.getColumnIndexOrThrow(RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL));
            rssItem = new RssItem(title, description, pubDate, imageUrl, itemId);
        }
        if (c != null) {
            c.close();
        }

        db.close();

        if (rssItem != null) {
            callback.onRssItemLoaded(rssItem);
        } else {
            callback.onDataNotAvailable();
        }
    }

    @Override
    public void saveRssItem(@NonNull RssItem rssItem) {
        checkNotNull(rssItem);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID, rssItem.getId());
        values.put(RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE, rssItem.getTitle());
        values.put(RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION, rssItem.getDescription());
        values.put(RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE, rssItem.getPubDate());
        values.put(RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL, rssItem.getImageUrl());

        db.insert(RssPersistenceContract.RssItemEntry.TABLE_NAME, null, values);

        db.close();
    }

    private static final String INSERT = "insert into "
            + RssPersistenceContract.RssItemEntry.TABLE_NAME + " ("
            + RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID + ", "
            + RssPersistenceContract.RssItemEntry.COLUMN_NAME_TITLE + ", "
            + RssPersistenceContract.RssItemEntry.COLUMN_NAME_DESCRIPTION + ", "
            + RssPersistenceContract.RssItemEntry.COLUMN_NAME_PUB_DATE + ", "
            + RssPersistenceContract.RssItemEntry.COLUMN_NAME_IMAGE_URL + ") values (?, ?, ?, ?, ?)";

    @Override
    public void saveFeed(@NonNull List<RssItem> list) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();

        try {
            SQLiteStatement insert = db.compileStatement(INSERT);
            for (RssItem rssItem : list) {
                insert.bindString(1, rssItem.getId());
                insert.bindString(2, rssItem.getTitle());
                insert.bindString(3, rssItem.getDescription());
                insert.bindString(4, rssItem.getPubDate());
                insert.bindString(5, rssItem.getImageUrl() != null ? rssItem.getImageUrl() : "");
                insert.executeInsert();
            }
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        db.close();
    }

    @Override
    public void refreshFeed() {
        // Not required because the {@link RssRepository} handles the logic of refreshing the
        // tasks from all the available data sources.
    }

    @Override
    public void deleteFeed() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(RssPersistenceContract.RssItemEntry.TABLE_NAME, null, null);

        db.close();
    }

    @Override
    public void deleteRssItem(@NonNull String rssItemId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = RssPersistenceContract.RssItemEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = {rssItemId};

        db.delete(RssPersistenceContract.RssItemEntry.TABLE_NAME, selection, selectionArgs);

        db.close();
    }
}
