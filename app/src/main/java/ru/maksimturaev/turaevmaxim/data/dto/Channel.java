package ru.maksimturaev.turaevmaxim.data.dto;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "channel", strict = false)
public class Channel {
    @ElementList(inline = true, name="item")
    private List<FeedItem> feedItems;

    public List<FeedItem> getFeedItems() {
        return feedItems;
    }

    public Channel() {
    }

    public Channel(List<FeedItem> feedItems) {
        this.feedItems = feedItems;
    }
}
