package ru.maksimturaev.turaevmaxim.data;


import retrofit2.Call;
import retrofit2.http.GET;
import ru.maksimturaev.turaevmaxim.data.dto.Feed;

public interface Api {
    @GET("/rss")
    Call<Feed> getFeed();
}
