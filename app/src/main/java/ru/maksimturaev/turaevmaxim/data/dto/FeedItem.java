package ru.maksimturaev.turaevmaxim.data.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item", strict = false)
public class FeedItem {
    @Element(name = "pubDate")
    private String pubDate;
    @Element(name = "title")
    private String title;
    @Element(name = "enclosure", required = false)
    private Enclosure link;
    @Element(name = "description")
    private String description;

    public FeedItem() {
    }

    public FeedItem(String description, Enclosure link, String title, String pubDate) {
        this.description = description;
        this.link = link;
        this.title = title;
        this.pubDate = pubDate;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Enclosure getLink() {
        return link;
    }

    public void setLink(Enclosure link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}