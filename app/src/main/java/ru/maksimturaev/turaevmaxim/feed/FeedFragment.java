/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.maksimturaev.turaevmaxim.R;
import ru.maksimturaev.turaevmaxim.data.RssItem;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Display a grid of {@link RssItem}s. User can choose to view all, active or completed rss items.
 */
public class FeedFragment extends Fragment implements FeedContract.View {

    private FeedContract.Presenter presenter;

    private FeedAdapter feedAdapter;

    private View noFeedView;

    private ImageView noFeedIcon;

    private TextView noFeedMainView;


    private RecyclerView listView;

    public FeedFragment() {
        // Requires empty public constructor
    }

    public static FeedFragment newInstance() {
        return new FeedFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        feedAdapter = new FeedAdapter(getActivity(), new ArrayList<RssItem>(0), mItemListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setPresenter(@NonNull FeedContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.result(requestCode, resultCode);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.feed_fragment, container, false);

        // Set up rss items view
        listView = (RecyclerView) root.findViewById(R.id.feed_list);
        listView.setAdapter(feedAdapter);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));


        // Set up  no rss items view
        noFeedView = root.findViewById(R.id.noFeed);
        noFeedIcon = (ImageView) root.findViewById(R.id.noFeedIcon);
        noFeedMainView = (TextView) root.findViewById(R.id.noFeedMain);

        // Set up progress indicator
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(listView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadFeed(true);
            }
        });

        return root;
    }

    /**
     * Listener for clicks on rss items in the ListView.
     */
    RssItemListener mItemListener = new RssItemListener() {
        @Override
        public void onRssItemClick(RssItem clickedRssItem) {
            presenter.openRssItemDetails(clickedRssItem);
        }
    };

    @Override
    public void setLoadingIndicator(final boolean active) {

        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });
    }

    @Override
    public void showFeed(List<RssItem> rssItems) {
        feedAdapter.replaceData(rssItems);

        listView.setVisibility(View.VISIBLE);
        noFeedView.setVisibility(View.GONE);
    }


    @Override
    public void showNoFeed() {
        showNoFeedViews(
                getResources().getString(R.string.no_feed),
                R.drawable.ic_assignment_turned_in_24dp,
                false
        );
    }


    @Override
    public void showSuccessfullySavedMessage() {
        showMessage(getString(R.string.successfully_saved_rss_item_message));
    }

    private void showNoFeedViews(String mainText, int iconRes, boolean showAddView) {
        listView.setVisibility(View.GONE);
        noFeedView.setVisibility(View.VISIBLE);

        noFeedMainView.setText(mainText);
        noFeedIcon.setImageDrawable(getResources().getDrawable(iconRes));
    }

    @Override
    public void showRssItemDetailsUi(String rssItemId) {
        // in it's own Activity, since it makes more sense that way and it gives us the flexibility
        // to show some Intent stubbing.
    }

    @Override
    public void showLoadingFeedError() {
        showMessage(getString(R.string.loading_feed_error));
    }

    private void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    public interface RssItemListener {
        void onRssItemClick(RssItem clickedRssItem);
    }

}
