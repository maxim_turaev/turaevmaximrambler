package ru.maksimturaev.turaevmaxim.feed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.maksimturaev.turaevmaxim.R;
import ru.maksimturaev.turaevmaxim.data.RssItem;

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class FeedAdapter extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static ArrayList<clickedState> itemClickedState;

    private enum clickedState {
        SHOW_PRIMARY_CONTENT,
        SHOW_SECONDARY_CONTENT
    }

    // Store a member variable for the contacts
    private List<RssItem> feed;
    // Store the context for easy access
    private Context context;

    private FeedFragment.RssItemListener itemListener;

    // Pass in the contact array into the constructor
    public FeedAdapter(Context context, List<RssItem> rssFeed, FeedFragment.RssItemListener itemListener) {
        this.feed = rssFeed;
        this.context = context;
        this.itemListener = itemListener;
        itemClickedState = new ArrayList<>();
        for (int i = 0; i < rssFeed.size(); i++) {
            itemClickedState.add(i, clickedState.SHOW_PRIMARY_CONTENT);
        }
    }

    public void replaceData(List<RssItem> rssItems) {
        itemClickedState.clear();
        feed.clear();
        for (int i = 0; i < rssItems.size(); i++) {
            itemClickedState.add(i, clickedState.SHOW_PRIMARY_CONTENT);
        }
        feed.addAll(rssItems);
        notifyDataSetChanged();
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return context;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View v1 = inflater.inflate(R.layout.feed_item, parent, false);
        View v2 = inflater.inflate(R.layout.feed_item_expanded, parent, false);
        if (viewType == 0) {
            return new ViewHolderPrimary(context, v1);
        } else {
            return new ViewHolderSecondary(context, v2);
        }
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        final RssItem feedItem = feed.get(position);

        // Set item views based on your views and data model

        if (viewHolder.getItemViewType() == 0) {
            ViewHolderPrimary viewHolderPrimary = (ViewHolderPrimary) viewHolder;
            TextView title = viewHolderPrimary.title;
            title.setText(feedItem.getTitle());
            ImageView imageView = viewHolderPrimary.imageView;
            if (feedItem.getImageUrl() == null || feedItem.getImageUrl().isEmpty()) {
                imageView.setImageResource(R.drawable.image);
            } else {
                Picasso.with(context).load(feedItem.getImageUrl()).placeholder(R.drawable.image).error(R.drawable.image).into(imageView);
            }
        } else if (viewHolder.getItemViewType() == 1) {
            ViewHolderSecondary viewHolderSecondary = (ViewHolderSecondary) viewHolder;
            TextView title = viewHolderSecondary.title;
            title.setText(feedItem.getTitle());
            TextView description = viewHolderSecondary.description;
            description.setText(feedItem.getDescription());
            ImageView imageView = viewHolderSecondary.imageView;
            if (feedItem.getImageUrl() == null || feedItem.getImageUrl().isEmpty()) {
                imageView.setImageResource(R.drawable.image);
            } else {
                Picasso.with(context).load(feedItem.getImageUrl()).placeholder(R.drawable.image).error(R.drawable.image).into(imageView);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (itemClickedState.get(position) == clickedState.SHOW_SECONDARY_CONTENT)
            return 1;
        return 0;
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return feed.size();
    }


    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolderSecondary extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView title;
        public TextView description;
        public ImageView imageView;
        private Context ctx;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolderSecondary(Context context, View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            ctx = context;
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickedState.set(getAdapterPosition(), itemClickedState.get(getAdapterPosition()) == clickedState.SHOW_PRIMARY_CONTENT ? clickedState.SHOW_SECONDARY_CONTENT : clickedState.SHOW_PRIMARY_CONTENT);
            notifyItemChanged(getAdapterPosition());
        }
    }

    public class ViewHolderPrimary extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView title;
        public ImageView imageView;
        private Context ctx;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolderPrimary(Context context, View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            ctx = context;
            title = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickedState.set(getAdapterPosition(), itemClickedState.get(getAdapterPosition()) == clickedState.SHOW_PRIMARY_CONTENT ? clickedState.SHOW_SECONDARY_CONTENT : clickedState.SHOW_PRIMARY_CONTENT);
            notifyItemChanged(getAdapterPosition());
        }
    }

}