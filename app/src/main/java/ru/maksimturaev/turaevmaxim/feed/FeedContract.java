/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.feed;

import android.support.annotation.NonNull;

import java.util.List;

import ru.maksimturaev.turaevmaxim.BasePresenter;
import ru.maksimturaev.turaevmaxim.BaseView;
import ru.maksimturaev.turaevmaxim.data.RssItem;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface FeedContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showFeed(List<RssItem> rssItems);

        void showRssItemDetailsUi(String rssItemId);

        void showLoadingFeedError();

        void showNoFeed();

        void showSuccessfullySavedMessage();

        boolean isActive();

    }

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void loadFeed(boolean forceUpdate);

        void openRssItemDetails(@NonNull RssItem requestedRssItem);
    }
}
