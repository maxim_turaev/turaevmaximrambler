package ru.maksimturaev.turaevmaxim.feed;

import dagger.Module;
import dagger.Provides;

/**
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link FeedPresenter}.
 */
@Module
public class FeedPresenterModule {

    private final FeedContract.View view;

    public FeedPresenterModule(FeedContract.View view) {
        this.view = view;
    }

    @Provides
    FeedContract.View provideFeedContractView() {
        return view;
    }

}
