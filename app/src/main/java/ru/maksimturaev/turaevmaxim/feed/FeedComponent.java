package ru.maksimturaev.turaevmaxim.feed;

import dagger.Component;
import ru.maksimturaev.turaevmaxim.data.source.RssRepositoryComponent;
import ru.maksimturaev.turaevmaxim.util.FragmentScoped;

/**
 * This is a Dagger component. Refer to {@link RssApplication} for the list of Dagger components
 * used in this application.
 * <P>
 * Because this component depends on the {@link RssRepositoryComponent}, which is a singleton, a
 * scope must be specified. All fragment components use a custom scope for this purpose.
 */
@FragmentScoped
@Component(dependencies = RssRepositoryComponent.class, modules = FeedPresenterModule.class)
public interface FeedComponent {
	
    void inject(FeedActivity activity);
}
