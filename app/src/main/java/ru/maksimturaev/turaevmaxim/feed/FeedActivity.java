/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.feed;

import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import ru.maksimturaev.turaevmaxim.R;
import ru.maksimturaev.turaevmaxim.RssApplication;
import ru.maksimturaev.turaevmaxim.util.ActivityUtils;
import ru.maksimturaev.turaevmaxim.util.EspressoIdlingResource;

public class FeedActivity extends AppCompatActivity {

    @Inject FeedPresenter feedPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_activity);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FeedFragment feedFragment =
                (FeedFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (feedFragment == null) {
            // Create the fragment
            feedFragment = FeedFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), feedFragment, R.id.contentFrame);
        }

        // Create the presenter
        DaggerFeedComponent.builder()
                .rssRepositoryComponent(((RssApplication) getApplication()).getRssRepositoryComponent())
                .feedPresenterModule(new FeedPresenterModule(feedFragment)).build()
                .inject(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }
}
