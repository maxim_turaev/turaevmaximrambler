/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.maksimturaev.turaevmaxim.feed;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import ru.maksimturaev.turaevmaxim.data.RssItem;
import ru.maksimturaev.turaevmaxim.data.source.RssDataSource;
import ru.maksimturaev.turaevmaxim.data.source.RssRepository;
import ru.maksimturaev.turaevmaxim.util.EspressoIdlingResource;

import static dagger.internal.Preconditions.checkNotNull;


/**
 * Listens to user actions from the UI ({@link FeedFragment}), retrieves the data and updates the
 * UI as required.
 * <p/>
 * By marking the constructor with {@code @Inject}, Dagger injects the dependencies required to
 * create an instance of the FeedPresenter (if it fails, it emits a compiler error).  It uses
 * {@link FeedPresenterModule} to do so.
 * <p/>
 * Dagger generated code doesn't require public access to the constructor or class, and
 * therefore, to ensure the developer doesn't instantiate the class manually and bypasses Dagger,
 * it's good practice minimise the visibility of the class/constructor as much as possible.
 **/
final class FeedPresenter implements FeedContract.Presenter {

    private final RssRepository rssRepository;

    private final FeedContract.View feedView;

    private boolean firstLoad = true;

    /**
     * Dagger strictly enforces that arguments not marked with {@code @Nullable} are not injected
     * with {@code @Nullable} values.
     */
    @Inject
    FeedPresenter(RssRepository rssRepository, FeedContract.View feedView) {
        this.rssRepository = rssRepository;
        this.feedView = feedView;
    }

    /**
     * Method injection is used here to safely reference {@code this} after the object is created.
     * For more information, see Java Concurrency in Practice.
     */
    @Inject
    void setupListeners() {
        feedView.setPresenter(this);
    }

    @Override
    public void start() {
        loadFeed(false);
    }

    @Override
    public void result(int requestCode, int resultCode) {
        // If a task was successfully added, show snackbar
        feedView.showSuccessfullySavedMessage();
    }

    @Override
    public void loadFeed(boolean forceUpdate) {
        // Simplification for sample: a network reload will be forced on first load.
//        loadFeed(forceUpdate || firstLoad, true);
        loadFeed(forceUpdate, true);
        firstLoad = false;
    }

    /**
     * @param forceUpdate   Pass in true to refresh the data in the {@link RssDataSource}
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    private void loadFeed(boolean forceUpdate, final boolean showLoadingUI) {
        if (showLoadingUI) {
            feedView.setLoadingIndicator(true);
        }
        if (forceUpdate) {
            rssRepository.refreshFeed();
        }

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment(); // App is busy until further notice

        rssRepository.getFeed(new RssDataSource.LoadFeedCallback() {
            @Override
            public void onFeedLoaded(List<RssItem> rssItems) {
                // This callback may be called twice, once for the cache and once for loading
                // the data from the server API, so we check before decrementing, otherwise
                // it throws "Counter has been corrupted!" exception.
                if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                    EspressoIdlingResource.decrement(); // Set app as idle.
                }

                // The view may not be able to handle UI updates anymore
                if (!feedView.isActive()) {
                    return;
                }

                if (showLoadingUI) {
                    feedView.setLoadingIndicator(false);
                }

                processFeed(rssItems);
            }

            @Override
            public void onDataNotAvailable() {
                // The view may not be able to handle UI updates anymore
                if (!feedView.isActive()) {
                    return;
                }
                feedView.showLoadingFeedError();
            }
        });
    }

    private void processFeed(List<RssItem> rssItems) {
        if (rssItems.isEmpty()) {
            // Show a message indicating there are no rssItems for that filter type.
            processEmptyFeed();
        } else {
            // Show the list of rssItems
            feedView.showFeed(rssItems);
            // Set the filter label's text.
        }
    }

    private void processEmptyFeed() {
        feedView.showNoFeed();
    }

    @Override
    public void openRssItemDetails(@NonNull RssItem requestedRssItem) {
        checkNotNull(requestedRssItem, "requestedRssItem cannot be null!");
        feedView.showRssItemDetailsUi(requestedRssItem.getId());
    }

}
