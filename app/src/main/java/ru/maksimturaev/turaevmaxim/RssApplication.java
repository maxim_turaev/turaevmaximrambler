package ru.maksimturaev.turaevmaxim;

import android.app.Application;

import ru.maksimturaev.turaevmaxim.data.NetworkModule;
import ru.maksimturaev.turaevmaxim.data.source.DaggerRssRepositoryComponent;
import ru.maksimturaev.turaevmaxim.data.source.RssRepositoryComponent;
import timber.log.Timber;

/**
 * Even though Dagger2 allows annotating a {@link dagger.Component} as a singleton, the code itself
 * must ensure only one instance of the class is created. Therefore, we create a custom
 * {@link Application} class to store a singleton reference to the {@link
 * RssRepositoryComponent}.
 * <p>
 * The application is made of 5 Dagger components, as follows:<BR />
 * {@link RssRepositoryComponent}: the data (it encapsulates a db and server data)<BR />
 * {@link FeedComponent}: showing the list of to do items, including marking them as
 * completed<BR />
 */
public class RssApplication extends Application {

    private RssRepositoryComponent repositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        repositoryComponent = DaggerRssRepositoryComponent.builder()
                .applicationModule(new ApplicationModule((getApplicationContext())))
                .networkModule(new NetworkModule("https://lenta.ru"))
                .build();

    }

    public RssRepositoryComponent getRssRepositoryComponent() {
        return repositoryComponent;
    }

}
